<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 11/14/2017
 * Time: 12:38 AM
 */
 ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>welcome to login </title>
<!--    <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/login.css">
    <style type="text/css">
        .error{
    padding-bottom: 5px;
            padding-top: 5px;
            border-radius: 0px;
        }
        span#logo{

            margin: 0;
            text-shadow: 2px 2px 3px rgba(111, 108, 108, 0.6);
            font-size: 42px;
            margin-left: -8px;
            font-weight: 700;

        }
         .navbar-brand {
    color: #26617d;
    margin-left: 23%;
             margin-bottom: 2%;

        }

          .navbar-brand:hover{
    color: #4c99ab;
}

        .navbar-brand img{
    display: inline-block;
}
        hr{
    border-color: #4e9aac;
        }
        a:hover{
    text-decoration: none;
        }
    </style>
</head>
<body  >





<div class="container">
    <h1 class="welcome text-center"> </h1>
    <div class="card card-container">

        <!--<h2 class='login_title text-center'>Login</h2>-->
        <a href="/" class="navbar-brand">


        </a>
        <hr>
        <p id="message" class="text-center alert  alert-danger error" hidden> </p>

        <form class="form-signin" id="loginform" method="post" action="success.php">
            <span id="reauth-email" class="reauth-email"></span>
            <p class="input_title">Full Name</p>
            <input type="text" id="fullname" name="fullname" class="login_box" placeholder="Sandeep Maurya ">
            <p class="input_title">Email</p>
            <input type="text" id="inputEmail" name="email" class="login_box" placeholder="sample@commerce.com" required autofocus>
            <p class="input_title">Password</p>
            <input type="password" id="inputPassword" name="password" class="login_box" placeholder="*****************" required>
            <div id="remember" class="checkbox">
                <label>

                </label>
            </div>
            <button class="btn btn-lg btn-primary" id="login" type="button">Login</button>
        </form><!-- /form -->
    </div><!-- /card-container -->
</div><!-- /container -->

<script src="/js/jquery.min.js"></script>

<script>

    $(function () {

        /**
         * input validation
         */

        $("#login").on("click",function () {

            var fullname = $("#fullname").val();

            var fullnamearr = fullname.split(" ");

            var email = $("#inputEmail").val();
            var password = $("#inputPassword").val();
            var error = false;

            if(fullname ==null || fullname == "" ||  fullname ==undefined){
                Error("message","Full name is Empty !",3000);
                $("#fullname").focus();
                error = true;

                return false;
            }

            if(fullnamearr.length<2){

                Error("message","Full name must be atleast 2 words !",3000);
                $("#fullname").focus();
                error = true;

                return false;
            }





            if(email == null || email == ""){

                Error("message","Email is empty !",3000);

                $("#inputEmail").focus();
                error = true;

                return false;

            }

            if( !isValidEmailAddress( email ) ) {

                Error("message","Email is Invalid !",3000);

                error =true;

                return false;

            }

            if(password == "" || password == null){

                Error("message","Password is empty !",3000);

                $("#inputPassword").focus();
                error =true;

                return false;

            }

            if(password.length<4){

                Error("message","Password must be atleast 4 characters !",3000);

                $("#inputPassword").focus();
                error =true;

                return false;

            }



            if(!error){

                $("#loginform").submit();


            }


        });
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }




    /**
     * function for error show
     */

    function Error(id,message,time) {

        $("#"+id).text(message).show();

        setTimeout(function () {
            $("#"+id).hide();
        },time);
    }


</script>



</body>
</html>