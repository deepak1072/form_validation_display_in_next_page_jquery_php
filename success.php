<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 11/14/2017
 * Time: 12:41 AM
 */

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>welcome to login </title>
    <!--    <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/login.css">
    <style type="text/css">
        html,body{
            background: radial-gradient(ellipse at center, rgba(185,210,224,1) 0%,rgba(187,214,228,1) 0%,rgba(186,211,225,1) 15%,rgba(186,211,225,1) 38%,rgba(169,199,215,1) 68%,rgba(169,199,215,1) 68%,rgba(169,199,215,1) 82%,rgba(158,191,208,1) 100%);
        }
        .error{
            padding-bottom: 5px;
            padding-top: 5px;
            border-radius: 0px;
        }
        span#logo{

            margin: 0;
            text-shadow: 2px 2px 3px rgba(111, 108, 108, 0.6);
            font-size: 42px;
            margin-left: -8px;
            font-weight: 700;

        }
        .navbar-brand {
            color: #26617d;
            margin-left: 23%;
            margin-bottom: 2%;

        }

        .navbar-brand:hover{
            color: #4c99ab;
        }

        .navbar-brand img{
            display: inline-block;
        }
        hr{
            border-color: #4e9aac;
        }
        a:hover{
            text-decoration: none;
        }
    </style>
</head>
<body  >
<div class="container">
    <div class=" " style="margin: 0px auto 25px;">
        <div class="col-xs-12 text-center" style="margin-top: 40px;background: #fcfcfc;padding: 10px;">
            <?php
                if($_SERVER["REQUEST_METHOD"] == "POST") :
                    $fullname = trim($_POST['fullname']);
                    $email = trim($_POST['email']);
                    $password = trim($_POST['password']);

                    ?>

            <h1>you successfully submitted following credentials </h1>


             <p> Full Name :  <?php echo $fullname ; ?></p>
            <p>Email id  :  <?php echo $email ; ?></p>

            <p>Password : <?php echo $password ;?></p>


                <?php else:  ?>
             <?php header("location: index.php") ;?>
            <?php endif;?>

        </div>
    </div>
</div>
</body>
</html>




